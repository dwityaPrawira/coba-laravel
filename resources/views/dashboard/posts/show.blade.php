@extends('dashboard.layouts.main')

@section('container')
<div class="container text-align: center">
    <div class="row">
        <div class="row my-5">
            <div class="col-lg-8">
                <h2 class="mb-3">{{ $post->title }}</h2>

                <a href="/dashboard/post" class="btn btn-success"><span data-feather="arrow-left"></span> Back to my all post</a>
                <a href="" class="btn btn-warning"><span data-feather="edit"></span> Edit</a>
                <a href="" class="btn btn-danger"><span data-feather="x-circle"></span> Delete</a>
                
                <img src="https://source.unsplash.com/random/1200x400?{{ $post->category->name }}" 
                    alt="{{ $post->category->name }}" class="img-fluid mt-3">
            
                <article class="my-3 fs-5">
                    {!! $post->body !!}
                </article>
                <a href="/posts" class="d-block mt-3">Back To Blog</a>
            </div> 
        </div>
    </div>
</div>
@endsection