@extends('dashboard.layouts.main')

@section('container')
<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
  <h2>My Post</h2>
      
</div>

<div class="table-responsive col-lg-8">
  <a href="/dashboard/post/create" class="btn btn-primary mb-3" >Create new Post</a>
  <table class="table table-striped table-sm">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Category</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach ($posts as $post)
      <tr>
        <td>{{ $loop->iteration }}</td>
        <td>{{ $post->title }}</td>
        <td>{{ $post->category->name }}</td>
        <td>
          <a class="badge bg-info" href="/dashboard/post/{{ $post->slug }}"><span data-feather="eye"></span></a>
          <a class="badge bg-warning" href=""><span data-feather="edit"></span></a>
          <a class="badge bg-danger" href=""><span data-feather="x-circle"></span></a>
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@endsection