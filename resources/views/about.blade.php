@extends('layout.main')

@section('container')
    <h2>Ini Halaman About me</h2>
    <h3>{{ $name }}</h3>
    <p>{{ $email }}</p>
    <img src="img/{{ $image }}" alt="{{ $name }}" width="200" >
@endsection
