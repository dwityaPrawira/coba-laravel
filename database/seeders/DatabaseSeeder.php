<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        User::factory(3)->create();

        // User::create([
        //     'name' => 'Shandika Galih',
        //     'email' => 'shandika@gmail.com',
        //     'password' => bcrypt('123456')
        // ]);

        // User::create([
        //     'name' => 'Dwitya Prawira',
        //     'email' => 'dwitya@gmail.com',
        //     'password' => bcrypt('123456')
        // ]);

        Category::create([
            'name' => 'Web Programming',
            'slug' => 'web-programming'
        ]);

        Category::create([
            'name' => 'Web Desain',
            'slug' => 'web-desain'
        ]);

        Category::create([
            'name' => 'Personal',
            'slug' => 'personal'
        ]);

        

        Post::factory(20)->create();

        // Post::create([
        //     'title' => 'Post pertama',
        //     'slug' => 'post-pertama',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur?',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur? Doloremque eaque repellat dolorem ad, a nemo sed eum non nam officiis magnam eos ipsam ab iste totam eius provident itaque quo eveniet ipsum nobis? Quaerat accusantium fugit cum vero. Molestias quam beatae similique laborum debitis, recusandae minima aspernatur possimus iste. Eveniet odit, ea ad facere adipisci explicabo blanditiis incidunt hic non! Dolor repellat odit nam cumque recusandae assumenda molestias cupiditate dignissimos repudiandae et nobis, culpa voluptatum quia magnam, totam voluptatibus sunt, earum pariatur. Recusandae explicabo similique iusto eligendi perspiciatis, minus enim! Atque, suscipit. Quae, nulla odit. Quisquam.',
        //     'user_id' => 1,
        //     'category_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Post kedua',
        //     'slug' => 'post-ke-dua',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur?',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur? Doloremque eaque repellat dolorem ad, a nemo sed eum non nam officiis magnam eos ipsam ab iste totam eius provident itaque quo eveniet ipsum nobis? Quaerat accusantium fugit cum vero. Molestias quam beatae similique laborum debitis, recusandae minima aspernatur possimus iste. Eveniet odit, ea ad facere adipisci explicabo blanditiis incidunt hic non! Dolor repellat odit nam cumque recusandae assumenda molestias cupiditate dignissimos repudiandae et nobis, culpa voluptatum quia magnam, totam voluptatibus sunt, earum pariatur. Recusandae explicabo similique iusto eligendi perspiciatis, minus enim! Atque, suscipit. Quae, nulla odit. Quisquam.',
        //     'user_id' => 1,
        //     'category_id' => 1
        // ]);

        // Post::create([
        //     'title' => 'Post ketiga',
        //     'slug' => 'post-ke-tiga',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur?',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur? Doloremque eaque repellat dolorem ad, a nemo sed eum non nam officiis magnam eos ipsam ab iste totam eius provident itaque quo eveniet ipsum nobis? Quaerat accusantium fugit cum vero. Molestias quam beatae similique laborum debitis, recusandae minima aspernatur possimus iste. Eveniet odit, ea ad facere adipisci explicabo blanditiis incidunt hic non! Dolor repellat odit nam cumque recusandae assumenda molestias cupiditate dignissimos repudiandae et nobis, culpa voluptatum quia magnam, totam voluptatibus sunt, earum pariatur. Recusandae explicabo similique iusto eligendi perspiciatis, minus enim! Atque, suscipit. Quae, nulla odit. Quisquam.',
        //     'user_id' => 2,
        //     'category_id' => 2
        // ]);

        // Post::create([
        //     'title' => 'Post keempat',
        //     'slug' => 'post-ke-empat',
        //     'excerpt' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur?',
        //     'body' => 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Illo quam nobis consequatur? Doloremque eaque repellat dolorem ad, a nemo sed eum non nam officiis magnam eos ipsam ab iste totam eius provident itaque quo eveniet ipsum nobis? Quaerat accusantium fugit cum vero. Molestias quam beatae similique laborum debitis, recusandae minima aspernatur possimus iste. Eveniet odit, ea ad facere adipisci explicabo blanditiis incidunt hic non! Dolor repellat odit nam cumque recusandae assumenda molestias cupiditate dignissimos repudiandae et nobis, culpa voluptatum quia magnam, totam voluptatibus sunt, earum pariatur. Recusandae explicabo similique iusto eligendi perspiciatis, minus enim! Atque, suscipit. Quae, nulla odit. Quisquam.',
        //     'user_id' => 2,
        //     'category_id' => 2
        // ]);

    }
}
