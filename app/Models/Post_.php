<?php 

namespace App\Models;

class Post {
    private static $blog_posts = [
        [
            "title" => "Judul post pertama",
            "slug" => "judul-post-pertama",
            "author" => "Dwityapraw",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Non voluptatibus in est ipsam. Nulla repudiandae sint alias, quod iusto aliquid. Doloribus dolor harum voluptates ab consequatur quis expedita? A, quibusdam?"
        ],
        [
            "title" => "Judul post kedua",
            "slug" => "judul-post-kedua",
            "author" => "bambank",
            "body" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptatum dolorum perspiciatis velit ab aut eos fugit? Vero, dolor rem. Maiores recusandae cumque esse autem saepe enim inventore excepturi accusantium hic debitis mollitia minima laborum, illo quas quod et assumenda necessitatibus atque ratione illum at blanditiis consectetur amet magni? Dolor autem eligendi, aliquid error ut maxime quisquam inventore laudantium quidem! Eligendi possimus debitis laboriosam ipsam perferendis veritatis deleniti, modi fugiat harum vero eum numquam repellat facilis blanditiis ut soluta natus, voluptatibus molestiae, repudiandae labore commodi? Architecto enim sint, est tenetur fuga rerum, incidunt ut distinctio saepe animi provident praesentium? Accusantium, labore."
        ]
    ];

    public static function all(){
        return collect(self::$blog_posts);
    }

    public static function find($slug){
        // $posting = self::$blog_posts;
        // $new_post = [];
        // foreach($posting as $p) {
        //     if($p["slug"] === $slug) {
        //         $new_post = $p;
        //     }
        // }

        $posts = static::all();
        return $posts->firstWhere('slug', $slug);
    }
}